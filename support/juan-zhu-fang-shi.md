# 捐助方式

### 捐助方式

> 注意！！转账后请**务必**联系晨旭，不然，你的记录可能会被遗漏掉

#### 微信捐助

![](../.gitbook/assets/wechat.png)

#### 支付宝捐助

![](../.gitbook/assets/alipay.jpg)

#### 海外捐助

[前往paypal](https://www.paypal.me/chenxuuu)

