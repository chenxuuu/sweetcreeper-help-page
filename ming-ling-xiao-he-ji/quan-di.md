---
description: 插件的部分汉化由晨旭翻译，如有错误请反馈
---

# 圈地

* 每人最多可拥有三块领地
* 圈地使用木棍，左击+右击两个点即可选定区域
* 用箭点击可查看领地信息
* 详细命令见下表

| 指令 | 解释 |
| :--- | :--- |
| /rp claim `领地名称` | 圈地 |
| /rp fl | 设置领地具体权限 |
| /rp wel | 设置领地欢迎语句 |
| /rp ls | 查看你自己的所有领地 |
| /rp del | 删除领地 |
| /rp b | 显示领地边界 |
| /rp rn | 重命名领地名称 |
| /rp am `玩家名` | 添加玩家到这个领地 |
| /rp aa `玩家名` | 设置玩家为这个领地管理员 |
| /rp ra `玩家名` | 取消玩家这个领地管理员权限 |
| /rp rm `玩家名` | 添加玩家从这个领地删除 |
| /rp k `玩家名` | 将玩家踢出领地并暂时拒绝其进入 |

