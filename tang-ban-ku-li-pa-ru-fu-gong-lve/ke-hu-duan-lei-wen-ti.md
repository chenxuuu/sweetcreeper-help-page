---
description: 客户端请使用糖拌整合的客户端版本，已整合高清修复与光影引擎（自带数个光影模型）、动态环绕mod、物品管理JEI、小地图
---

# 客户端下载

蓝奏云（不限速）：

[糖拌苦力怕-mod归来v10.2正式版(1.12.2)](https://www.lanzous.com/b00t8mlne)

OneDrive转直链下载（推荐海外）：

[糖拌苦力怕-mod归来v10.2正式版(1.12.2)](https://1dv.papapoi.com/%E7%B3%96%E6%8B%8C%E8%8B%A6%E5%8A%9B%E6%80%95-mod%E5%BD%92%E6%9D%A5v10.2%E6%AD%A3%E5%BC%8F%E7%89%88%281.12.2%29.7z)

## 注意

客户端内自带一个**mod一键更新器**，请在第一次下载客户端后运行一次，更新糖拌当前正在使用的mod

如果中途某天游玩没法进服务器，也说明需要更新mod了，运行一次即可

已有mod：

* [拔刀剑](http://www.mcbbs.net/thread-726664-1-1.html)
* [拔刀剑附加](http://www.mcbbs.net/thread-710736-1-1.html)
* [匠魂](http://www.mcbbs.net/thread-661201-1-1.html)
* [匠魂附加](http://www.mcbbs.net/thread-731337-1-18.html)
* [匠魂拔刀剑联动](http://www.mcbbs.net/thread-846907-1-1.html)
* [神秘时代6](http://www.mcbbs.net/thread-776706-1-1.html)
* [植物魔法](http://www.mcbbs.net/thread-722470-1-1.html)
* [额外植物学](http://www.mcbbs.net/thread-596279-1-1.html)
* [料理工艺](http://www.mcbbs.net/thread-821999-1-1.html)
* [盖亚魔典](http://www.mcbbs.net/thread-679274-1-1.html)
* [家具](http://www.mcbbs.net/thread-321693-1-1.html)
* [电脑](http://computercraft.info/wiki/)
* [车万女仆](https://www.mcbbs.net/thread-882845-1-5.html)
* [凿子](https://www.mcbbs.net/thread-641383-1-5.html)
* [建筑师工艺](https://www.mcbbs.net/thread-686580-1-1.html)
